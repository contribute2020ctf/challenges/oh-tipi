Rails.application.routes.draw do
  devise_for :users
  get 'secret', to: 'topsecret#secret'
  get '2ndfactor', to: 'tfa#show'
  post '2ndfactor', to: 'tfa#verify'
  root to: 'topsecret#secret'
  # For details on the DSL available within this file, see https://guides.rubyonrails.org/routing.html
end
