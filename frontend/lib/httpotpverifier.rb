require 'httparty'

BASE_URI = ENV['OTP_ENDPOINT']

class HTTPOTPVerifier
  def self.check_code(user, code)
    base_uri = BASE_URI
    path = "/otp/verify/#{user.id}/#{code}"
    HTTParty.get(base_uri + path)
  end
end
