class TfaController < ApplicationController
  
  before_action :authenticate_user!
  def show

  end

  def verify 
    code = params[:code]
    result = HTTPOTPVerifier.check_code(current_user, code)
    if result.code == 200  
      session["2ndfactor"] = true
      redirect_to '/secret'
      return
    else 
      session["2ndfactor"] = false
      redirect_to '/'
      return
    end
  end
end
