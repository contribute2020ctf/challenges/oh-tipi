class TopsecretController < ApplicationController
  before_action :ensure_2nd_factor
  def secret
    render plain: ENV["FLAG1"]
  end
  private

  def ensure_2nd_factor
    unless session["2ndfactor"] == true
      flash[:error] = "You must provide your second factor to access this section"
      redirect_to  '/2ndfactor'
  end
 end
end
