package main

import (
	"fmt"
	"github.com/gorilla/pat"
	"github.com/jinzhu/gorm"
	_ "github.com/jinzhu/gorm/dialects/sqlite"
	"github.com/xlzd/gotp"
	"log"
	"net/http"
)

type User struct {
	gorm.Model
	Secret string
	Name   string
}

var mux = pat.New()
var db *gorm.DB

func healthPage(w http.ResponseWriter, r *http.Request) {
	fmt.Fprintf(w, "Status: OK!")
	fmt.Println("Endpoint Hit: healthPage")
}

func verifyOTP(w http.ResponseWriter, r *http.Request) {
	// TBD
	userName := r.URL.Query().Get(":username")
	code := r.URL.Query().Get(":code")
	fmt.Println("Endpoint Hit: verifyOTP")
	user := User{}
	db.Where("name = ?", userName).Find(&user)
	if user.Name != "" {
		totp := gotp.NewDefaultTOTP(user.Secret)
		if code == totp.Now() {
			w.WriteHeader(http.StatusOK)
			fmt.Fprintf(w, "OK %s %s", totp.Now(), code)
			return
		}
		w.WriteHeader(http.StatusForbidden)
		fmt.Fprintf(w, "WRONG %s %s", totp.Now(), code)
		return
	}
	w.WriteHeader(http.StatusForbidden)
	fmt.Fprintf(w, "Fail")

}
func init() {

	var err error
	db, err = gorm.Open("sqlite3", ":memory:")
	if err != nil {
		panic("failed to connect database")
	}

	// Mock admin user :D
	db.AutoMigrate(&User{})
	secretLength := 16
	secret := gotp.RandomSecret(secretLength)
	db.Create(&User{Secret: secret, Name: "admin"})

	mux.Get("/otp/verify/{username}/{code}", verifyOTP)
	mux.Get("/health", healthPage)
}

func main() {

	defer db.Close()

	log.Fatal(http.ListenAndServe(":10000", mux))
}
