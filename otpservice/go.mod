module gitlab.com/contribute2020ctf/challenges/oh-tipi

go 1.13

require (
	github.com/gorilla/context v1.1.1 // indirect
	github.com/gorilla/mux v1.7.3 // indirect
	github.com/gorilla/pat v1.0.1
	github.com/jinzhu/gorm v1.9.12
	github.com/xlzd/gotp v0.0.0-20181030022105-c8557ba2c119
)
